import glob from "glob"
import fs from "fs"
import path from "path"
import { getType } from "mime"
import putObjectWithRetry from "../utils/putObjectWithRetry"

const getUploadFolder = (s3: AWS.S3) => {
  const getFiles = (dir: string): string[] => {
    const contents = glob.sync(dir + "/**/*")
    const files = contents.filter(
      (file: string) => !fs.statSync(file).isDirectory()
    )
    return files
  }

  const uploadFolder = async (
    sourceDir: string,
    targetDir: string,
    bucket: string
  ) => {
    const allFiles = getFiles(sourceDir)
    const sourceBasename = path.basename(sourceDir)
    const putObjects = allFiles.map((file: string) => {
      const key = path.join(
        targetDir,
        sourceBasename,
        file.split(sourceBasename).pop() || ""
      )
      return putObjectWithRetry(
        s3,
        key,
        fs.createReadStream(file),
        bucket,
        getType(file) || undefined
      )
    })
    return Promise.all(putObjects)
  }

  return uploadFolder
}

export default getUploadFolder
