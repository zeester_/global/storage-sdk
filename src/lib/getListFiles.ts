const getListFiles = (s3: AWS.S3) => {
  const listFiles = async (path: string, bucket: string) => {
    const data = await s3
      .listObjectsV2({
        Bucket: bucket,
        Prefix: path,
      })
      .promise()

    let token = data.NextContinuationToken

    while (token) {
      const newData = await s3
        .listObjectsV2({
          Bucket: bucket,
          Prefix: path,
          ContinuationToken: token,
        })
        .promise()

      data.Contents = data.Contents?.concat(newData?.Contents ?? [])
      token = newData.NextContinuationToken
    }

    return data
  }

  return listFiles
}

export default getListFiles
