import fs from "fs"
import path from "path"
import { getType } from "mime"
import putObjectWithRetry from "../utils/putObjectWithRetry"

const getUploadFile = (s3: AWS.S3) => {
  const uploadFile = async (
    sourceFile: string,
    targetDir: string,
    bucket: string
  ) => {
    const sourceBasename = path.basename(sourceFile)
    return putObjectWithRetry(
      s3,
      path.join(targetDir, sourceBasename),
      fs.createReadStream(sourceFile),
      bucket,
      getType(sourceFile) || undefined
    )
  }

  return uploadFile
}

export default getUploadFile
