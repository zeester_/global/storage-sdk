import AWS from "aws-sdk"

import getUploadFolder from "./getUploadFolder"
import getUploadFile from "./getUploadFile"
import getDeleteFile from "./getDeleteFile"
import getDeleteFolder from "./getDeleteFolder"
import getCopyFile from "./getCopyFile"
import getCopyFolder from "./getCopyFolder"
import getMoveFile from "./getMoveFile"
import getMoveFolder from "./getMoveFolder"
import getListFiles from "./getListFiles"

type S3Params = {
  accessKeyId: string
  secretAccessKey: string
  endpoint?: string
  region?: string
}

function setup({ accessKeyId, secretAccessKey, endpoint, region }: S3Params) {
  const s3 = new AWS.S3({
    accessKeyId,
    secretAccessKey,
    s3ForcePathStyle: true,
    endpoint: endpoint || "https://obj-gravscale.zadarazios.com:443",
    region: region || "SP2",
    httpOptions: {
      timeout: 10000000
    }
  })

  return {
    uploadFolder: getUploadFolder(s3),
    uploadFile: getUploadFile(s3),
    deleteFile: getDeleteFile(s3),
    deleteFolder: getDeleteFolder(s3),
    copyFile: getCopyFile(s3),
    copyFolder: getCopyFolder(s3),
    moveFile: getMoveFile(s3),
    moveFolder: getMoveFolder(s3),
    listFiles: getListFiles(s3),
  }
}

export default setup
