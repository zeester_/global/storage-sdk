import getListFiles from "./getListFiles"
import copySourceWithRetry from "../utils/copySourceWithRetry"

const getCopyFolder = (s3: AWS.S3) => {
  const copyFolder = async (
    from: string,
    to: string,
    fromBucket: string,
    toBucket: string
  ) => {
    const listFiles = getListFiles(s3)
    const listObjectsResponse = await listFiles(from, fromBucket)

    const folderContentInfo = listObjectsResponse.Contents ?? []
    let folderPrefix = listObjectsResponse?.Prefix ?? ""
    folderPrefix = !folderPrefix.endsWith("/")
      ? folderPrefix + "/"
      : folderPrefix

    await Promise.all(
      folderContentInfo.map(async (fileInfo) => {
        const source = `${fromBucket}/${fileInfo.Key}`
        const destination = `${to}/${fileInfo.Key?.replace(folderPrefix, "")}`
        await copySourceWithRetry(s3, source, destination, toBucket)
      })
    )
  }

  return copyFolder
}

export default getCopyFolder
