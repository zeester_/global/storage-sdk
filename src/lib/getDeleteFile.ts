import path from "path"

const getDeleteFile = (s3: AWS.S3) => {
  const deleteFile = async (
    fileName: string,
    targetDir: string,
    bucket: string
  ) => {
    return s3
      .deleteObject({
        Bucket: bucket,
        Key: path.join(targetDir, fileName),
      })
      .promise()
  }

  return deleteFile
}

export default getDeleteFile
