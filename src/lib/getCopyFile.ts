import copySourceWithRetry from "../utils/copySourceWithRetry"

const getCopyFile = (s3: AWS.S3) => {
  const copyFile = async (
    from: string,
    to: string,
    fromBucket: string,
    toBucket: string
  ) => {
    const source = fromBucket !== toBucket ? `${fromBucket}/${from}` : from
    await copySourceWithRetry(s3, source, to, toBucket)
  }

  return copyFile
}

export default getCopyFile
