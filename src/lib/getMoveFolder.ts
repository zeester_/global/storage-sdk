import copySourceWithRetry from "../utils/copySourceWithRetry"
import getListFiles from "./getListFiles"

const getMoveFolder = (s3: AWS.S3) => {
  const moveFolder = async (
    from: string,
    to: string,
    fromBucket: string,
    toBucket: string
  ) => {
    const listFiles = getListFiles(s3)
    const listObjectsResponse = await listFiles(from, fromBucket)

    const folderContentInfo = listObjectsResponse.Contents ?? []
    let folderPrefix = listObjectsResponse?.Prefix ?? ""
    folderPrefix = !folderPrefix.endsWith("/")
      ? folderPrefix + "/"
      : folderPrefix

    await Promise.all(
      folderContentInfo.map(async (fileInfo) => {
        const source = `${fromBucket}/${fileInfo.Key}`
        const destination = `${to}/${fileInfo.Key?.replace(folderPrefix, "")}`
        await copySourceWithRetry(s3, source, destination, toBucket)

        await s3
          .deleteObject({
            Bucket: fromBucket,
            Key: fileInfo?.Key ?? "",
          })
          .promise()
      })
    )
  }

  return moveFolder
}

export default getMoveFolder
