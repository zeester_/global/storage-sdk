import copySourceWithRetry from "../utils/copySourceWithRetry"

const getMoveFile = (s3: AWS.S3) => {
  const moveFile = async (
    from: string,
    to: string,
    fromBucket: string,
    toBucket: string
  ) => {
    const source = fromBucket !== toBucket ? `${fromBucket}/${from}` : from
    await copySourceWithRetry(s3, source, to, toBucket)

    await s3
      .deleteObject({
        Bucket: fromBucket,
        Key: from,
      })
      .promise()
  }

  return moveFile
}

export default getMoveFile
