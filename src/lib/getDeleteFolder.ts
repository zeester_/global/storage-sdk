const getDeleteFolder = (s3: AWS.S3) => {
  const deleteFolder = async (targetDir: string, bucket: string) => {
    const listParams = {
      Bucket: bucket,
      Prefix: targetDir,
    }

    const listedObjects = await s3.listObjectsV2(listParams).promise()

    if (listedObjects.Contents?.length === 0) return

    const deleteParams = {
      Bucket: bucket,
      Delete: { Objects: Array<AWS.S3.ObjectIdentifier>() },
    }

    listedObjects.Contents?.forEach(({ Key }: AWS.S3.Object) => {
      deleteParams.Delete.Objects.push({ Key: Key || "" })
    })

    await s3.deleteObjects(deleteParams).promise()

    if (listedObjects.IsTruncated) await deleteFolder(targetDir, bucket)
  }

  return deleteFolder
}

export default getDeleteFolder
