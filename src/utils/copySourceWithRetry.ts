import { sleep } from "./sleep"

const MAX_ATTEMPTS = 10

const copySourceWithRetry = async (
  s3: AWS.S3,
  source: string,
  destination: string,
  destinationBucket: string,
  attempt: number = 1
) => {
  try {
    await s3
      .copyObject({
        Bucket: destinationBucket,
        CopySource: source,
        Key: destination,
      })
      .promise()
  } catch (err) {
    if (attempt >= MAX_ATTEMPTS) {
      throw new Error("Could not copy object, exceeded max attempts")
    }
    await copySourceWithRetry(
      s3,
      source,
      destination,
      destinationBucket,
      attempt + 1
    )
  }
}

export default copySourceWithRetry
