import { sleep } from "./sleep"

const MAX_ATTEMPTS = 10

const putObjectWithRetry = async (
  s3: AWS.S3,
  key: string,
  body: AWS.S3.Body,
  bucket: string,
  contentType?: string,
  attempt: number = 1
) => {
  try {
    await s3
      .putObject({
        Bucket: bucket,
        Key: key,
        Body: body,
        ContentType: contentType,
      })
      .promise()
  } catch (err) {
    if (attempt >= MAX_ATTEMPTS) {
      throw new Error("Could not put object, exceeded max attempts")
    }
    await putObjectWithRetry(s3, key, body, bucket, contentType, attempt + 1)
  }
}

export default putObjectWithRetry
